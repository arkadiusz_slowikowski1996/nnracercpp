// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VehicleWheel.h"
#include "NNRacerCppWheelRear.generated.h"

UCLASS()
class UNNRacerCppWheelRear : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UNNRacerCppWheelRear();
};



