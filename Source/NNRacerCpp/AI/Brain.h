#pragma once

#include "CoreMinimal.h"
#include "Raycasting.h"
//#include "Intelligent.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Vector.h"
#include "NeuralNetwork.h"
//#include "BrainToTurnOffEntry.h"
#include "Runtime/Core/Public/Containers/Queue.h"
#include "Components/ActorComponent.h"
#include "Brain.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class NNRACERCPP_API UBrain : public UActorComponent//, public IIntelligent
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere)
		float currentRunTime = 0;
	UPROPERTY(VisibleAnywhere)
		bool finished = false;
	UPROPERTY(VisibleAnywhere)
		int32 index = 0;
	UPROPERTY(VisibleAnywhere)
		FNeuralNetwork network = FNeuralNetwork();
	UPROPERTY(VisibleAnywhere)
		FTimerHandle fixedUpdateHandler = FTimerHandle();
	UPROPERTY(VisibleAnywhere)
		FTimerHandle killWhenIdleHandler = FTimerHandle();

	UBrain();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	float GetDistanceCovered();
	void TurnOff(bool doOff, bool finished);
	UFUNCTION()
		void KillWhenIdle();
	UFUNCTION()
		void KillWhenRotating();
	void Activate();
	UFUNCTION()
		void FixedUpdate();
	void ChangeScore(double delta);

	FORCEINLINE URaycasting* GetRaycasting() { return raycasting; }
	FORCEINLINE void SetRaycasting(URaycasting* raycasting) { this->raycasting = raycasting; }
	FORCEINLINE int32 GetScore() { return this->score; }
	FORCEINLINE void SetScore(int32 score) { this->score = score; }
	FORCEINLINE void SetTurnedOn(bool turnedOn) { this->turnedOn = turnedOn; }

protected:
	UPROPERTY(VisibleAnywhere)
		double score = 0;

	virtual void BeginPlay() override;

private:

	UPROPERTY(EditAnywhere)
		URaycasting* raycasting;
	UPROPERTY(VisibleAnywhere)
		FVector prevLoc = FVector();
	UPROPERTY(VisibleAnywhere)
		FVector loc5SecAgo = FVector();
	UPROPERTY(VisibleAnywhere)
		FVector pos5SecAgo = FVector();

	bool turnedOn = true;

	void SetSteer(float value);
	void SetThrottle(float value);
};