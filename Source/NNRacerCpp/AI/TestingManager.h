#pragma once

#include "CoreMinimal.h"
#include "Brain.h"
#include "AI/BotsManager.h"
#include "TestingManager.generated.h"

UCLASS()
class NNRACERCPP_API UTestingManager : public UBotsManager
{
	GENERATED_BODY()

public:
	void HandleBrainDied(int index) override;
	void Activate() override;
	void Deactivate() override;

private:
	UPROPERTY(VisibleAnywhere)
		UBrain* mainBrain;
	int32 localBrainsPopulation = 1;

	void SpawnBrain();
};