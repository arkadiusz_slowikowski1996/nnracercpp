#include "TestingManager.h"
#include "NeuralNetwork.h"
#include "GeneticManager.h"

void UTestingManager::HandleBrainDied(int index)
{
	SpawnBrain();
}

void UTestingManager::Activate()
{
	UBotsManager::brainsPopulation = localBrainsPopulation;
	FNeuralNetwork::Setup(NNinputs, NNoutputs, NNhiddenLayers, NNhiddenNeurons);
	SpawnBrain();

	TArray<UBrain*> newBrain = TArray<UBrain*>();
	UGeneticManager::CreateNewGeneration(newBrain, 1);
	mainBrain = newBrain[0];
}

void UTestingManager::Deactivate()
{
	if (mainBrain)
		mainBrain->GetOwner()->Destroy();
}

void UTestingManager::SpawnBrain()
{
	//TODO: set actor active
	mainBrain->network.CopyNetworkFrom(UGeneticManager::theBestNetwork);
	mainBrain->GetOwner()->SetActorLocation(startActor->GetActorLocation());
	mainBrain->GetOwner()->SetActorRotation(startActor->GetActorRotation());
}