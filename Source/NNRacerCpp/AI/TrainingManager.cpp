#include "TrainingManager.h"
#include "GeneticManager.h"
#include "Engine.h"
#include "Brain.h"

UTrainingManager::UTrainingManager()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;
	SetComponentTickEnabled(true);
}

void UTrainingManager::BeginPlay()
{
	Super::BeginPlay();
}

void UTrainingManager::HandleBrainDied(int index)
{
	isDead[index] = true;

	if (GEngine)
		//GEngine->AddOnScreenDebugMessage(-1, 3.0f, GetNumberOfDead() == brainsPopulation ? FColor::Red : FColor::Green, FString("UTrainingManager::HandleBrainDied"));
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, FString(FString::SanitizeFloat(GetNumberOfDead()) + " " + FString::SanitizeFloat(brainsPopulation)));

	if (GetNumberOfDead() == brainsPopulation)
		HandleNewGeneraion();
}

void UTrainingManager::Activate()
{
	UBotsManager::Activate();

	UBotsManager::brainsPopulation = localBrainsPopulation;
	isDead.SetNum(brainsPopulation);
	UGeneticManager::Setup(GetWorld(), ABrainedBotCar::StaticClass());
	FNeuralNetwork::Setup(NNinputs, NNoutputs, NNhiddenLayers, NNhiddenNeurons);

	HandleNewGeneraion();
}

void UTrainingManager::Deactivate()
{
	UGeneticManager::DestroyBrainObjects();
}

void UTrainingManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	/*int32 deadNumber = 0;

	for (UBrain* brain : UGeneticManager::currentBrains)
	{
		if (!brain->turnedOn) deadNumber++;
	}*/

	//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Black, FString("deads: " + FString::SanitizeFloat(deadNumber)));
	/*if (deadNumber == brainsPopulation)
		HandleNewGeneraion();*/
}

int32 UTrainingManager::GetNumberOfDead()
{
	int32 returnValue = 0;

	for (bool b : isDead)
		if (b) returnValue++;

	return returnValue;
}

void UTrainingManager::HandleNewGeneraion()
{
	TArray<UBrain*> newBrains = TArray<UBrain*>();
	UGeneticManager::CreateNewGeneration(newBrains, brainsPopulation);
	for (size_t i = 0; i < newBrains.Num(); i++)
	{
		newBrains[i]->GetOwner()->SetActorLocation(startActor->GetActorLocation(), false, nullptr, ETeleportType::TeleportPhysics);
		newBrains[i]->GetOwner()->SetActorRotation(startActor->GetActorRotation(), ETeleportType::TeleportPhysics);
		newBrains[i]->index = i;
	}

	isDead.Empty();
	isDead.SetNum(brainsPopulation);
}