#pragma once

#include "CoreMinimal.h"
#include "Brain.h"
#include "UObject/Interface.h"
#include "Intelligent.generated.h"

UINTERFACE(MinimalAPI)
class UIntelligent : public UInterface
{
	GENERATED_BODY()
};

class NNRACERCPP_API IIntelligent
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		 UBrain* GetBrainComponent();
};