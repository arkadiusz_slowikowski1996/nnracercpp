#include "BotsManager.h"
#include "BrainedBotCar.h"
#include "RacerGameMode.h"
#include "Engine.h"

UBotsManager::UBotsManager()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UBotsManager::BeginPlay()
{
	Super::BeginPlay();
}

void UBotsManager::HandleBrainDied(int32 index)
{
}

void UBotsManager::Activate()
{
	NNinputs = 5;
	NNoutputs = 2;
	NNhiddenLayers = 2;
	NNhiddenNeurons = 7;
	brainsPopulation = 0;
}

void UBotsManager::Deativate()
{
}

void UBotsManager::EnqueueBrainToTurnOff(UBrain* brain, bool turnOff, bool finished)
{
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 3.0, FColor::Green, FString("UBotsManager::EnqueueBrainToTurnOff"));

	FBrainToTurnOffEntry entry = FBrainToTurnOffEntry(brain, turnOff, finished);
	brainsToTurnOff.Enqueue(entry);
}

void UBotsManager::TurnOffEnqueuedBrains()
{
	while (!brainsToTurnOff.IsEmpty())
	{
		FBrainToTurnOffEntry entry = FBrainToTurnOffEntry();
		brainsToTurnOff.Dequeue(entry);

		entry.brain->finished = entry.finished && entry.turnOff;
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 3.0, entry.turnOff ? FColor::Red : FColor::Green, FString("UBotsManager::TurnOffEnqueuedBrains"));
		AActor* owner = entry.brain->GetOwner();
		owner->SetActorHiddenInGame(entry.turnOff);
		owner->SetActorEnableCollision(!entry.turnOff);
		((ABrainedBotCar*)owner)->GetMesh()->SetNotifyRigidBodyCollision(!entry.turnOff);
		owner->SetActorTickEnabled(!entry.turnOff);
		entry.brain->GetRaycasting()->SetIsTurnedOn(!entry.turnOff);
		entry.brain->SetTurnedOn(!entry.turnOff);

		//if (GetWorld() && ((ARacerGameMode*)GetWorld()->GetAuthGameMode()) && ((ARacerGameMode*)GetWorld()->GetAuthGameMode())->currentManager)
		if (entry.turnOff)
		{
			entry.brain->ChangeScore(-entry.brain->GetDistanceCovered());
			HandleBrainDied(entry.brain->index);
			if (GetWorld())
			{
				GetWorld()->GetTimerManager().ClearTimer(entry.brain->fixedUpdateHandler);
				GetWorld()->GetTimerManager().ClearTimer(entry.brain->killWhenIdleHandler);
			}
		}
		else
		{
			entry.brain->Activate();
		}
	}
}

void UBotsManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	TurnOffEnqueuedBrains();
}