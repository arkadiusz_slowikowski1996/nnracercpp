#pragma once

#include "CoreMinimal.h"
#include "AI/Wall.h"
#include "RaceMeta.generated.h"

UCLASS()
class NNRACERCPP_API ARaceMeta : public AWall
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
};