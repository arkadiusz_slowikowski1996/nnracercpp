#include "Wall.h"
#include "BrainedBotCar.h"
#include "RacerGameMode.h"
#include "Vector.h"
#include "Engine.h"
#include "Brain.h"

AWall::AWall()
{
	PrimaryActorTick.bCanEverTick = true;
	rootSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root component"));
	rootSceneComponent->SetupAttachment(GetRootComponent());

	SetRootComponent(rootSceneComponent);

	wallMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall mesh"));
	wallMesh->RelativeLocation = FVector(50, 50, 50);
	wallMesh->SetupAttachment(RootComponent);
}

void AWall::BeginPlay()
{
	Super::BeginPlay();
	wallMesh->SetSimulatePhysics(false);
	wallMesh->SetNotifyRigidBodyCollision(true);
	wallMesh->SetCollisionProfileName("BlockAll");
	wallMesh->OnComponentBeginOverlap.AddDynamic(this, &AWall::OnOverlapBegin);
	wallMesh->OnComponentHit.AddDynamic(this, &AWall::OnCompHit);
}

void AWall::OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor->IsA(ABrainedBotCar::StaticClass()))
	{
		HandleActorsInteraction(OtherActor);
	}
}

void AWall::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->IsA(ABrainedBotCar::StaticClass()))
	{
		HandleActorsInteraction(OtherActor);
	}
}

void AWall::HandleActorsInteraction(AActor * OtherActor)
{
	ABrainedBotCar* brainedCar = (ABrainedBotCar*)OtherActor;
	TArray<UBrain*> componentsInActor;
	brainedCar->GetComponents<UBrain>(componentsInActor);
	//GEngine->AddOnScreenDebugMessage(-1, 3, componentsInActor[0] ? FColor::Green : FColor::Red, FString("AWall::HandleActorsInteraction"));
	UBrain* brain = componentsInActor[0];
	
	((ARacerGameMode*)GetWorld()->GetAuthGameMode())->currentManager->EnqueueBrainToTurnOff(brain, true, isMeta);
	
	//brain->TurnOff(true, isMeta);
}

void AWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}