#pragma once

#include "CoreMinimal.h"
#include "GeneticManager.h"
#include "BrainedBotCar.h"
#include "NeuralNetwork.h"
#include "AI/BotsManager.h"
#include "TrainingManager.generated.h"

UCLASS()
class NNRACERCPP_API UTrainingManager : public UBotsManager
{
	GENERATED_BODY()

public:
	UTrainingManager();
	void HandleBrainDied(int index) override;
	void Activate() override;
	void Deactivate() override;

	int32 numberOfParents = 1;
	int32 numberOfMutations = 1;

protected:

	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	int32 localBrainsPopulation = 6;
	UPROPERTY(VisibleAnywhere)
		TArray<bool> isDead;

	int32 GetNumberOfDead();
	void HandleNewGeneraion();
};
