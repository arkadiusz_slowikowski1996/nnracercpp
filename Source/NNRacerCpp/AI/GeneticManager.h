#pragma once

#include "Brain.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "CoreMinimal.h"
#include "NeuralNetwork.h"
#include "BrainedBotCar.h"
#include "Components/ActorComponent.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GeneticManager.generated.h"

UCLASS()
class NNRACERCPP_API UGeneticManager : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	static FNeuralNetwork theBestNetwork;
	static TArray<UBrain*> currentBrains;
	static TSubclassOf<class APawn> classToSpawn;
	static UWorld* worldObject;

	static void Setup(UObject* worldContext, TSubclassOf<class APawn> classOfSpecimen);
	static void CreateNewGeneration(TArray<UBrain*>& result, int32 population);
	static void DestroyBrainObjects();

private:
	static void KillBrains(TArray<UBrain*>& brains);
	static void AdjustBrains(int32 population, TArray<FNeuralNetwork> data);
	static void Reproduce(TArray<UBrain*>& result, int32 population);
	static TArray<FNeuralNetwork> ChooseParents();
	static void SpawnAllOfTheBrains();
};