#include "Brain.h"
#include "BotsManager.h"
#include "RacerGameMode.h"
#include "Vector.h"
#include "Engine.h"
#include "NNRacerCppPawn.h"
#include "BrainedBotCar.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "WheeledVehicleMovementComponent.h"
#include "Runtime/Core/Public/GenericPlatform/GenericPlatformMisc.h"
#include "Runtime/Engine/Classes/AI/NavigationSystemBase.h"
#include "Runtime/NavigationSystem/Public/NavigationSystem.h"

//TQueue<UBrain*> UBrain::brainsToTurnOff = TQueue<UBrain*>;

UBrain::UBrain()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.TickInterval = 0.016f;
}

float UBrain::GetDistanceCovered()
{
	float returnValue = 0;

	if (GetWorld() && ((ARacerGameMode*)GetWorld()->GetAuthGameMode()) && ((ARacerGameMode*)GetWorld()->GetAuthGameMode())->currentManager)
	{
		AGameModeBase* gameMode = GetWorld()->GetAuthGameMode();
		ARacerGameMode* racerGameMode = (ARacerGameMode*)gameMode;
		UBotsManager* botsManager = racerGameMode->currentManager;
		FVector destination = botsManager->GetMetaLocation();

		UNavigationSystemV1* navSystem = UNavigationSystemV1::GetCurrent(GetWorld());
		navSystem->GetPathLength(GetOwner()->GetActorLocation(), destination, returnValue);
	}
	return returnValue;
}

void UBrain::TurnOff(bool doOff, bool finished)
{
//	brainsToTurnOff.Enqueue(BrainToTurnOffEntry(this, doOff, finished));

	//this->finished = finished && doOff;
	//if (GEngine)
	//	GEngine->AddOnScreenDebugMessage(-1, 3.0, doOff ? FColor::Red : FColor::Green, FString("UBrain::TurnOff"));
	//AActor* owner = GetOwner();
	//owner->SetActorHiddenInGame(doOff);
	//owner->SetActorEnableCollision(!doOff);
	//((ABrainedBotCar*)owner)->GetMesh()->SetNotifyRigidBodyCollision(!doOff);
	//owner->SetActorTickEnabled(!doOff);
	//raycasting->SetIsTurnedOn(!doOff);
	//SetTurnedOn(!doOff);

	////if (GetWorld() && ((ARacerGameMode*)GetWorld()->GetAuthGameMode()) && ((ARacerGameMode*)GetWorld()->GetAuthGameMode())->currentManager)
	//if (doOff)
	//{
	//	ChangeScore(-GetDistanceCovered());
	//	((ARacerGameMode*)GetWorld()->GetAuthGameMode())->currentManager->HandleBrainDied(index);
	//	if (GetWorld())
	//	{
	//		GetWorld()->GetTimerManager().ClearTimer(fixedUpdateHandler);
	//		GetWorld()->GetTimerManager().ClearTimer(killWhenIdleHandler);
	//	}
	//}
	//else
	//{
	//	Activate();
	//}
}

void UBrain::KillWhenIdle()
{
	if (!turnedOn ) return;

	if (FVector::Dist(GetOwner()->GetActorLocation(), prevLoc) < 100.0f)
	{
		TurnOff(true, false);
	}

	prevLoc = GetOwner()->GetActorLocation();
}

void UBrain::KillWhenRotating()
{
	if (!turnedOn) return;
	if (GetWorld() && ((ARacerGameMode*)GetWorld()->GetAuthGameMode()) && ((ARacerGameMode*)GetWorld()->GetAuthGameMode())->currentManager
		&& FVector::Dist(GetOwner()->GetActorLocation(), prevLoc) < 700.0f)
	{
		TurnOff(true, false);
		((ARacerGameMode*)GetWorld()->GetAuthGameMode())->currentManager->HandleBrainDied(index);
	}

	loc5SecAgo = GetOwner()->GetActorLocation();
}

void UBrain::Activate()
{
	if (GetWorld())
	{
		/*if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3.0, FColor::Blue, FString("INDEX: " + FString::SanitizeFloat(index)));*/
		//FTimerHandle killWhenRotatingHandler = FTimerHandle();
		GetWorld()->GetTimerManager().SetTimer(fixedUpdateHandler, this, &UBrain::FixedUpdate, 0.001f, turnedOn, 0.1f);
		GetWorld()->GetTimerManager().SetTimer(killWhenIdleHandler, this, &UBrain::KillWhenIdle, 1, turnedOn, 3);
		//GetWorld()->GetTimerManager().SetTimer(killWhenRotatingHandler, this, &UBrain::KillWhenRotating, 10, true, 5);
	}
}

void UBrain::BeginPlay()
{
	Super::BeginPlay();
}

void UBrain::ChangeScore(double delta)
{
	score += delta;
}

void UBrain::FixedUpdate()
{
	if (!turnedOn) return;

	TArray<double> inputs = TArray<double>();
	TArray<double> outputs = TArray<double>();

	int32 NNinputs = ((ARacerGameMode*)GetWorld()->GetAuthGameMode())->currentManager->NNinputs;
	int32 NNoutputs = ((ARacerGameMode*)GetWorld()->GetAuthGameMode())->currentManager->NNoutputs;

	inputs.SetNum(NNinputs);
	outputs.SetNum(NNoutputs);

	for (size_t i = 0; i < inputs.Num(); i++)
		inputs[i] = (raycasting->hitsResults[i].Distance > 1000 || !raycasting->hitsResults[i].bBlockingHit) ? 1 : (raycasting->hitsResults[i].Distance / 1000);

	outputs = network.Process(inputs);

	if (outputs.Num() > 0)
	{
		SetSteer(outputs[0]);
		SetThrottle(outputs[1]);
	}
}

void UBrain::SetSteer(float value)
{
	((ABrainedBotCar*)GetOwner())->MoveRight(value);
}

void UBrain::SetThrottle(float value)
{
	((ABrainedBotCar*)GetOwner())->MoveForward(value);
}

void UBrain::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	currentRunTime += DeltaTime;
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}