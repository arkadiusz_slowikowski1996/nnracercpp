#include "RacerGameMode.h"
#include "BotsManager.h"
#include "Engine.h"

ARacerGameMode::ARacerGameMode()
{
	DefaultPawnClass = APlayerObserver::StaticClass();
	trainingManager = NewObject<UTrainingManager>(this, TEXT("Training Manager"));
	testingManager = NewObject<UTestingManager>(this, TEXT("Testing Manager"));
}

void ARacerGameMode::BeginPlay()
{
	Super::BeginPlay();
	currentManager = trainingManager;
	currentManager->Activate();
}

void ARacerGameMode::SetStartActor(AActor* startActor)
{
	this->startActor = startActor;
	trainingManager->startActor = startActor;
	testingManager->startActor = startActor;
}

void ARacerGameMode::SetMetaActor(AActor* metaActor)
{
	this->metaActor = metaActor;
	trainingManager->metaActor = metaActor;
	testingManager->metaActor = metaActor;
}