#include "RaceStart.h"
#include "RacerGameMode.h"
#include "Engine.h"

ARaceStart::ARaceStart()
{
	PrimaryActorTick.bCanEverTick = true;
	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall mesh"));
	mesh->SetupAttachment(GetRootComponent());
}

void ARaceStart::BeginPlay()
{
	Super::BeginPlay();
	if (GetWorld() && GetWorld()->GetAuthGameMode())
		((ARacerGameMode*)GetWorld()->GetAuthGameMode())->SetStartActor(this);
}

void ARaceStart::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}