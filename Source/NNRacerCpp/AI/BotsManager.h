#pragma once

#include "CoreMinimal.h"
#include "Brain.h"
#include "Runtime/Core/Public/Containers/Queue.h"
#include "Components/ActorComponent.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "BotsManager.generated.h"

USTRUCT()
struct NNRACERCPP_API FBrainToTurnOffEntry
{
	GENERATED_USTRUCT_BODY()

		UBrain* brain = nullptr;

	bool turnOff = false;
	bool finished = false;

	FBrainToTurnOffEntry(UBrain* brain, bool turnOff, bool finished)
	{
		this->brain = brain;
		this->turnOff = turnOff;
		this->finished = finished;
	};
	FBrainToTurnOffEntry() {};
	~FBrainToTurnOffEntry() {};
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class NNRACERCPP_API UBotsManager : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
		AActor* startActor;
	UPROPERTY(EditAnywhere)
		AActor* metaActor;

	int32 brainsPopulation;
	int32 NNinputs;
	int32 NNoutputs;
	int32 NNhiddenLayers;
	int32 NNhiddenNeurons;

	UBotsManager();

	void EnqueueBrainToTurnOff(UBrain* brain, bool turnOff, bool finished);

	virtual void HandleBrainDied(int32 index);
	virtual void Activate();
	virtual void Deativate();

	FVector GetMetaLocation() { return this->metaActor->GetActorLocation(); }

protected:
	TQueue<FBrainToTurnOffEntry> brainsToTurnOff;

	void TurnOffEnqueuedBrains();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void BeginPlay() override;
};
