#pragma once

#include "TrainingManager.h"
#include "TestingManager.h"
//#include "Actor.h"
#include "BotsManager.h"
#include "PlayerObserver.h"
#include "CoreMinimal.h"
#include "NNRacerCppGameMode.h"
#include "RacerGameMode.generated.h"

UCLASS()
class NNRACERCPP_API ARacerGameMode : public ANNRacerCppGameMode
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere)
		UBotsManager* currentManager;

	ARacerGameMode();



	void SetStartActor(AActor* startActor);
	void SetMetaActor(AActor* metaActor);

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere)
		UTrainingManager* trainingManager;
	UPROPERTY(VisibleAnywhere)
		UTestingManager* testingManager;

	UPROPERTY(VisibleAnywhere)
		AActor* startActor;
	UPROPERTY(VisibleAnywhere)
		AActor* metaActor;
};