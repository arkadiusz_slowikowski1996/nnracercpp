#include "Raycasting.h"
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>
#include "Runtime/Engine/Public/DrawDebugHelpers.h"
#include "Runtime/Engine/Classes/Engine/World.h"

int32 URaycasting::i = 0;

URaycasting::URaycasting()
{
	PrimaryComponentTick.bCanEverTick = true;
	raycastDirs.SetNum(5);
	hitsResults.SetNum(5);
}

void URaycasting::BeginPlay()
{
	myI = i++;
	Super::BeginPlay();
}

void URaycasting::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	if (!isTurnedOn) return;

	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	/*raycastDirs[0] = GetOwner()->GetTransform().TransformPosition(GetComponentLocation() + FVector(0, 100, 0));
	raycastDirs[1] = GetOwner()->GetTransform().TransformPosition(GetComponentLocation() + FVector(100, 100, 0));
	raycastDirs[2] = GetOwner()->GetTransform().TransformPosition(GetComponentLocation() + FVector(100, 0, 0));
	raycastDirs[3] = GetOwner()->GetTransform().TransformPosition(GetComponentLocation() + FVector(100, -100, 0));
	raycastDirs[4] = GetOwner()->GetTransform().TransformPosition(GetComponentLocation() + FVector(0, -100, 0));*/

	raycastDirs[0] = GetComponentTransform().TransformPosition(FVector(0, 100000, 0));
	raycastDirs[1] = GetComponentTransform().TransformPosition(FVector(100000, 100000, 0));
	raycastDirs[2] = GetComponentTransform().TransformPosition(FVector(100000, 0, 0));
	raycastDirs[3] = GetComponentTransform().TransformPosition(FVector(100000, -100000, 0));
	raycastDirs[4] = GetComponentTransform().TransformPosition(FVector(0, -100000, 0));

	for (size_t i = 0; i < hitsResults.Num(); i++)
	{
		if (GetWorld())
		{
			raycastDirs[i].Z = GetComponentLocation().Z;
			GetWorld()->LineTraceSingleByObjectType(
				hitsResults[i],
				GetComponentLocation(),
				raycastDirs[i],
				ECollisionChannel::ECC_WorldStatic
			);

			/*GetWorld()->LineTraceSingleByProfile(
				hitsResults[i],
				GetComponentLocation(),
				raycastDirs[i],
				"BlockAll");*/
		}
	}

	//if (GEngine)
	//	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, FString::SanitizeFloat(myI));

	for (size_t i = 0; i < hitsResults.Num(); i++)
	{
		DrawDebugLine(
			GetWorld(),
			GetComponentLocation(),
			hitsResults[i].ImpactPoint,
			FColor::Red,
			false,
			.005f
		);
	}
}