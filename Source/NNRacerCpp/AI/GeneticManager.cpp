#include "GeneticManager.h"
#include "BotsManager.h"
#include "RacerGameMode.h"
#include "Engine.h"

FNeuralNetwork UGeneticManager::theBestNetwork = FNeuralNetwork();
TSubclassOf<class APawn> UGeneticManager::classToSpawn = TSubclassOf<class ABrainedBotCar>();
UWorld* UGeneticManager::worldObject = nullptr;
TArray<UBrain*>  UGeneticManager::currentBrains = TArray<UBrain*>();

void UGeneticManager::KillBrains(TArray<UBrain*>& brains)
{
	if (brains.Num() > 0)
	{
		int32 population = brains.Num();

		for (size_t i = 0; i < population; i++)
		{
			(brains)[i]->GetOwner()->Destroy();
		}
	}
}

void UGeneticManager::AdjustBrains(int32 population, TArray<FNeuralNetwork> data)
{
	UBotsManager* manager = ((ARacerGameMode*)worldObject->GetAuthGameMode())->currentManager;
	int32 numberOfMutations = manager->IsA(UTrainingManager::StaticClass()) ? ((UTrainingManager*)manager)->numberOfMutations : 0;

	for (size_t i = 0; i < population; i++)
	{
		((ABrainedBotCar*)currentBrains[i]->GetOwner())->brain->network = FNeuralNetwork(numberOfMutations, -1, data);
		((ABrainedBotCar*)currentBrains[i]->GetOwner())->brain->TurnOff(false, false);
	}
}

//UBrain* UGeneticManager::SpawnBrain(TArray<FNeuralNetwork> data, int32 parentToCopy, int32 index)
//{
//	FVector location(0.0f, 0.0f, 5000.0f);
//	FRotator rotation(0.0f, 0.0f, 0.0f);
//	FActorSpawnParameters spawnInfo;
//	ABrainedBotCar* newActor = (index >= currentBrains.Num()) ? worldObject->SpawnActor<ABrainedBotCar>(ABrainedBotCar::StaticClass(), location, rotation, spawnInfo)
//		: (ABrainedBotCar*)currentBrains[index]->GetOwner();
//	FNeuralNetwork newNetwork = FNeuralNetwork::FNeuralNetwork(data, numberOfMutations);
//
//	return newActor->brain;
//}

void UGeneticManager::Reproduce(TArray<UBrain*>& result, int32 population)
{
	TArray<FNeuralNetwork> parents = ChooseParents();

	if (parents.Num() > 0)
		theBestNetwork = (parents[0]);

	AdjustBrains(population, parents);
}

TArray<FNeuralNetwork> UGeneticManager::ChooseParents()
{
	UBotsManager* manager = ((ARacerGameMode*)worldObject->GetAuthGameMode())->currentManager;
	int32 numberOfParents = manager->IsA(UTrainingManager::StaticClass()) ? ((UTrainingManager*)manager)->numberOfParents : 0;

	TArray<FNeuralNetwork> parents;
	parents.SetNum(numberOfParents);

	for (size_t i = 0; i < parents.Num(); i++)
	{
		double maxValue = -DBL_MAX;
		size_t maxIndex = -1;

		for (size_t j = 0; j < currentBrains.Num(); j++)
		{
			if (currentBrains[j]->GetScore() > maxValue)
			{
				maxIndex = j;
				maxValue = currentBrains[j]->GetScore();
				//if (GEngine)
					//GEngine->AddOnScreenDebugMessage(-1, 10.0, FColor::Green, FString::SanitizeFloat(currentBrains[j]->GetScore()));
			}
			else
			{
				//if (GEngine)
					//GEngine->AddOnScreenDebugMessage(-1, 10.0, FColor::Red, FString::SanitizeFloat(currentBrains[j]->GetScore()));
			}
		}
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 3.0, FColor::Blue, FString::SanitizeFloat(currentBrains[maxIndex]->GetScore()));

		currentBrains[maxIndex]->SetScore(currentBrains[maxIndex]->GetScore() * 2);
		parents[i] = currentBrains[maxIndex]->network;
	}

	return parents;
}

void UGeneticManager::SpawnAllOfTheBrains()
{
	int32 population = 0;
	int32 numberOfMutations = 0;

	if (worldObject && (ARacerGameMode*)worldObject->GetAuthGameMode())
	{
		UBotsManager* manager = ((ARacerGameMode*)worldObject->GetAuthGameMode())->currentManager;
		population = manager->brainsPopulation;
		numberOfMutations = manager->IsA(UTrainingManager::StaticClass()) ? ((UTrainingManager*)manager)->numberOfMutations : 0;
	}

	for (size_t i = 0; i < population; i++)
	{
		FVector location(0.0f, 0.0f, 5000.0f);
		FRotator rotation(0.0f, 0.0f, 0.0f);
		FActorSpawnParameters spawnInfo;
		ABrainedBotCar* newActor = worldObject->SpawnActor<ABrainedBotCar>(ABrainedBotCar::StaticClass(), location, rotation, spawnInfo);
		newActor->brain->network = FNeuralNetwork(numberOfMutations, -1);
		newActor->brain->TurnOff(false, false);

		if (!currentBrains.Contains(newActor->brain)) currentBrains.Add(newActor->brain);
	}
}

void UGeneticManager::Setup(UObject* worldContext, TSubclassOf<class APawn> classOfSpecimen)
{
	currentBrains.Empty();
	UGeneticManager::worldObject = GEngine->GetWorldFromContextObject(worldContext);
	UGeneticManager::classToSpawn = classOfSpecimen;
}

void UGeneticManager::CreateNewGeneration(TArray<UBrain*>& result, int32 population)
{
	if (currentBrains.Num() == 0)
	{
		SpawnAllOfTheBrains();
	}
	else
		Reproduce(currentBrains, population);
	result = currentBrains;
}

void UGeneticManager::DestroyBrainObjects()
{
	KillBrains(currentBrains);
}
