// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "RaceStart.generated.h"

UCLASS()
class NNRACERCPP_API ARaceStart : public AActor
{
	GENERATED_BODY()
	
public:	
	ARaceStart();
	virtual void Tick(float DeltaTime) override;

protected:
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* mesh;

	virtual void BeginPlay() override;
};