#include "RaceMeta.h"
#include "RacerGameMode.h"

void ARaceMeta::BeginPlay()
{
	isMeta = true;

	if (GetWorld() && GetWorld()->GetAuthGameMode())
		((ARacerGameMode*)GetWorld()->GetAuthGameMode())->SetMetaActor(this);
}