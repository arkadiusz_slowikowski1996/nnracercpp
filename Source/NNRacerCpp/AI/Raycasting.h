#pragma once

#include "CoreMinimal.h"
#include "Vector.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Components/SceneComponent.h"
#include "Raycasting.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class NNRACERCPP_API URaycasting : public USceneComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere)
		TArray<FHitResult> hitsResults = TArray<FHitResult>();

	URaycasting();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	FORCEINLINE void SetIsTurnedOn(bool newIsTurnedOn) { isTurnedOn = newIsTurnedOn; }

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere)
		TArray<FVector> raycastDirs = TArray<FVector>();

	static int32 i;
	int32 myI;
	bool isTurnedOn = true;
};