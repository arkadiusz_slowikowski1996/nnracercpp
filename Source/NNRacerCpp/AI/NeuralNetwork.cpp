#include "NeuralNetwork.h"
#include <ctime>
#include "Engine/Engine.h"

int32 FNeuralNetwork::inputs = 0;
int32 FNeuralNetwork::outputs = 0;
int32 FNeuralNetwork::hiddenLayers = 0;
int32 FNeuralNetwork::hiddenNeuronsPerLayer = 0;

FNeuralNetwork::FNeuralNetwork()
{
}

FNeuralNetwork::FNeuralNetwork(int32 numberOfMutations, int32 parentToCopy, TArray<FNeuralNetwork> parents)// : FNeuralNetwork(false)
{
	InitNetwork();

	for (size_t i = 0; i < weights.Num(); i++)
		weights[i] = parents[parentToCopy == -1 ? FMath::RandRange(0, parents.Num() - 1) : parentToCopy].weights[i];

	if (parentToCopy == -1)
		for (size_t i = 0; i < numberOfMutations; i++)
			weights[FMath::RandRange(0, weights.Num() - 1)] = FMath::RandRange((float)-1, (float)1);
}

FNeuralNetwork::FNeuralNetwork(int32 numberOfMutations, int32 parentToCopy)// : FNeuralNetwork(false)
{
	InitNetwork();

	FRandomStream randomStream;

	for (size_t i = 0; i < weights.Num(); i++)
	{
		randomStream.GenerateNewSeed();
		FMath::RandInit(randomStream.GetCurrentSeed());
		float f = FMath::FRandRange((float)-1, (float)1);

		weights[i] = f;
	}
}

FNeuralNetwork::~FNeuralNetwork()
{

}

void FNeuralNetwork::Setup(int32 inputs, int32 outputs, int32 hiddenLayers, int32 hiddenNeuronsPerLayer)
{
	FNeuralNetwork::inputs = inputs;
	FNeuralNetwork::outputs = outputs;
	FNeuralNetwork::hiddenLayers = hiddenLayers;
	FNeuralNetwork::hiddenNeuronsPerLayer = hiddenNeuronsPerLayer;
}

void FNeuralNetwork::CopyNetworkFrom(FNeuralNetwork FNeuralNetwork)
{
	InitNetwork();

	for (size_t i = 0; i < weights.Num(); i++)
		weights[i] = FNeuralNetwork.weights[i];
}

TArray<double> FNeuralNetwork::Process(TArray<double> inputs)
{
	int32 weightsIndex = 0;
	TArray<double> prevLayer = inputs;

	for (size_t i = 1; i < layers.Num(); i++)
	{
		TArray<double> currentLayer = TArray<double>();
		currentLayer.SetNum(layers[i]);

		for (size_t j = 0; j < layers[i]; j++)
		{
			double sum = 0;

			for (size_t k = 0; k < prevLayer.Num(); k++)
			{
				sum += prevLayer[k] * weights[weightsIndex];
				weightsIndex++;
			}

			currentLayer[j] = FMath::Tan(sum);
		}

		prevLayer = currentLayer;
	}

	return prevLayer;
}

void FNeuralNetwork::InitNetwork()
{
	layers.SetNum(2 + hiddenLayers);

	for (size_t i = 0; i < layers.Num(); i++)
	{
		if (i == 0)
			layers[i] = inputs;
		else if (i == layers.Num() - 1)
			layers[i] = outputs;
		else
			layers[i] = hiddenNeuronsPerLayer;
	}

	int32 weightsNum = 0;
	for (size_t i = 1; i < layers.Num(); i++)
		weightsNum += layers[i - 1] * layers[i];

	weights = TArray<double>();
	weights.SetNum(weightsNum);
}