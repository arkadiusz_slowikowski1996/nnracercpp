#pragma once

#include "CoreMinimal.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "GameFramework/Actor.h"
#include "Wall.generated.h"

UCLASS()
class NNRACERCPP_API AWall : public AActor
{
	GENERATED_BODY()

public:
	AWall();
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere)
		bool isMeta = false;
	UFUNCTION()
		void OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* wallMesh;
	UPROPERTY(EditAnywhere)
		USceneComponent* rootSceneComponent;

	virtual void BeginPlay() override;

private:
	void HandleActorsInteraction(AActor* OtherActor);
};