#pragma once

#include "CoreMinimal.h"
#include "Runtime/CoreUObject/Public/UObject/Object.h"
#include "NeuralNetwork.generated.h"

USTRUCT()
struct FNeuralNetwork
{
	GENERATED_BODY()

public:
	static int32 inputs;
	static int32 outputs;
	static int32 hiddenLayers;
	static int32 hiddenNeuronsPerLayer;

	//FNeuralNetwork(bool doInit = false);
	FNeuralNetwork(int32 numberOfMutations, int32 parentToCopy, TArray<FNeuralNetwork> parents);
	FNeuralNetwork(int32 numberOfMutations, int32 parentToCopy);
	FNeuralNetwork();
	~FNeuralNetwork();
	static void Setup(int32 inputs, int32 outputs, int32 hiddenLayers, int32 hiddenNeuronsPerLayer);
	void CopyNetworkFrom(FNeuralNetwork FNeuralNetwork);
	TArray<double> Process(TArray<double> inputs);

	UPROPERTY()
	TArray<double> weights;
private:
	TArray<int32> layers;

	void InitNetwork();
};
