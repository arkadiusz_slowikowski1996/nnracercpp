// Fill out your copyright notice in the Description page of Project Settings.


#include "TestCollisionBox.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Engine.h"

// Sets default values
ATestCollisionBox::ATestCollisionBox()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ATestCollisionBox::OnHitActor(AActor * SelfActor, AActor * OtherActor, FVector NormalImpulse, const FHitResult & Hit)
{
	GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Purple, FString("ATestCollisionBox::OnHitActor"));
}

void ATestCollisionBox::OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Purple, FString(" ATestCollisionBox::OnHit"));
}

// Called when the game starts or when spawned
void ATestCollisionBox::BeginPlay()
{
	Super::BeginPlay();

	((UStaticMeshComponent*)GetRootComponent())->OnComponentHit.AddDynamic(this, &ATestCollisionBox::OnHit);
	((UStaticMeshComponent*)GetRootComponent())->SetNotifyRigidBodyCollision(true);

}

// Called every frame
void ATestCollisionBox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

