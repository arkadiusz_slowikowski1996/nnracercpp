// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "NNRacerCppGameMode.generated.h"

UCLASS(minimalapi)
class ANNRacerCppGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ANNRacerCppGameMode();
};



