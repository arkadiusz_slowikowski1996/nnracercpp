// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "NNRacerCppGameMode.h"
#include "NNRacerCppPawn.h"
#include "NNRacerCppHud.h"

ANNRacerCppGameMode::ANNRacerCppGameMode()
{
	DefaultPawnClass = ANNRacerCppPawn::StaticClass();
	HUDClass = ANNRacerCppHud::StaticClass();
}
